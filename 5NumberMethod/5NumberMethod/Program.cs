﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5NumberMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            double num = 0;

            numbers(num);

            Console.WriteLine(num);
            
                       
        }

        static double numbers(double num)
        {
            num = 0;

            List<double> stored = new List<double>();

            for (int i = 0; i < 5; i++)
            {
                num = double.Parse(Console.ReadLine());
                stored.Add(num);
            }

            num = stored[0] + stored[1] + stored[2] + stored[3] + stored[4];

            return num;
        }
    }
}
